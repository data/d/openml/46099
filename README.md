# OpenML dataset: Phishing_Email_Dataset

https://www.openml.org/d/46099

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

### Description:

The dataset named "phishing_email.csv" comprises email contents that have been classified into phishing or legitimate categories. Each row in the dataset is an email entry, containing two fields: `text_combined` and `label`. The `text_combined` field holds the entire content of an email, which may include the subject, body, and any embedded URLs, while the `label` fields classify the email as phishing (`1`) or legitimate (`0`).

### Attribute Description:

- **text_combined**: Contains a comprehensive dumped text of an email, amalgamating the subject, the body, and possibly URLs. The text is unstructured, potentially lengthy, and may exhibit a wide range of natural language features, including informal language, technical terminology, and various linguistic structures. Examples of content range from technical support emails, linguistic textbook descriptions, corporate summaries regarding energy market negotiations, to phishing schemes pretending to offer financial opportunities.
- **label**: A binary indicator with `1` representing a phishing email and `0` signifying a legitimate email. This classification serves as the dataset's target variable for predictive modeling tasks aimed at identifying phishing attempts.

### Use Case:

This dataset can significantly contribute to cybersecurity efforts, particularly in developing machine learning models capable of detecting and filtering phishing attempts from legitimate email communications. Researchers and developers can leverage the rich, varied content of the emails to train models that understand the nuances and patterns indicative of phishing. Additionally, linguistic analysts may find the dataset beneficial for studying language use in fraudulent versus legitimate emails, potentially uncovering linguistic markers that are characteristic of phishing attempts. Moreover, organizations focused on strengthening their email security protocols can use insights derived from this dataset to better educate their employees on recognizing and handling suspicious emails, ultimately reducing the risk of phishing attacks.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46099) of an [OpenML dataset](https://www.openml.org/d/46099). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46099/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46099/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46099/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

